use authority::*;
use std::marker::PhantomData;

pub trait Message {
    fn is_internal() -> bool;
    fn source(&self) -> &Source;
    fn destination(&self) -> &Destination;
}

pub trait Request : Message {}

pub trait Response<T> : Message where T: Request {
    fn serialised_request(&self) -> &SerialisedRequest<T>;
    fn error_code(&self) -> Option<u32>;
}

pub trait Accumulate<T> : Message + Sized {
    fn accumulate(messages: &Vec<Self>) -> Option<Self>;
}



#[derive(Debug, Clone)]
pub struct SerialisedRequest<T> where T: Request {
    data: Vec<u8>,
    tag: PhantomData<T>,
}

impl<T> SerialisedRequest<T> where T: Request {
    pub fn new(data: Vec<u8>) -> SerialisedRequest<T> {
        SerialisedRequest{ data: data, tag: PhantomData, }
    }

    pub fn data(&self) -> &Vec<u8> {
        &self.data
    }
}



#[derive(Debug, Clone)]
pub struct SerialisedResponse<T, U> where T: Response<U>, U: Request {
    data: Vec<u8>,
    tag: PhantomData<T>,
    unused: PhantomData<U>,
}

impl<T, U> SerialisedResponse<T, U> where T: Response<U>, U: Request {
    pub fn new(data: Vec<u8>) -> SerialisedResponse<T, U> {
        SerialisedResponse{ data: data, tag: PhantomData, unused: PhantomData, }
    }

    pub fn data(&self) -> &Vec<u8> {
        &self.data
    }
}
