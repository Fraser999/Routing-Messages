pub type PublicKey = String;

#[derive(Debug, RustcEncodable, RustcDecodable, PartialEq, PartialOrd, Eq, Ord, Clone, Hash)]
pub enum GroupAuthority {
    ClientManager(String),
    NaeManager(String),
    NodeManager(String),
}

#[derive(Debug, RustcEncodable, RustcDecodable, PartialEq, PartialOrd, Eq, Ord, Clone, Hash)]
pub enum SingleAuthority {
    ManagedNode(String),
    Client(String, PublicKey),
}

#[derive(Debug, RustcEncodable, RustcDecodable, PartialEq, PartialOrd, Eq, Ord, Clone, Hash)]
pub enum Source {
    Group(GroupAuthority, Vec<PublicKey>),
    Single(SingleAuthority),
}

impl Source {
    pub fn to_destination(&self) -> Destination {
        match self {
            &Source::Group(ref authority, _) => Destination::Group(authority.clone()),
            &Source::Single(ref authority) => Destination::Single(authority.clone()),
        }
    }
}

#[derive(Debug, RustcEncodable, RustcDecodable, PartialEq, PartialOrd, Eq, Ord, Clone, Hash)]
pub enum Destination {
    Group(GroupAuthority),
    Single(SingleAuthority),
}

impl Destination {
    pub fn to_single_source(&self) -> Source {
        match self {
            &Destination::Group(ref authority) => panic!(),
            &Destination::Single(ref authority) => Source::Single(authority.clone()),
        }
    }

    pub fn to_group_source(&self, public_keys: &Vec<PublicKey>) -> Source {
        match self {
            &Destination::Group(ref authority) => Source::Group(authority.clone(), public_keys.clone()),
            &Destination::Single(ref authority) => panic!(),
        }
    }
}

