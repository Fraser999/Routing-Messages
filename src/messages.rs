use authority::*;
use message_traits::*;

#[derive(Debug, Clone)]
pub struct GetRequest {
    source: Source,
    destination: Destination,
    data_name: String,
}

impl GetRequest {
    pub fn new(source: Source, destination: Destination, data_name: String) -> GetRequest {
        GetRequest{ source: source, destination: destination, data_name: data_name, }
    }
    pub fn data_name(&self) -> &String { &self.data_name }
}

impl Request for GetRequest {}

impl Message for GetRequest {
    fn is_internal() -> bool { false }
    fn source(&self) -> &Source { &self.source }
    fn destination(&self) -> &Destination { &self.destination }
}

impl Accumulate<GetRequest> for GetRequest {
    fn accumulate(_messages: &Vec<Self>) -> Option<Self> { None }
}

impl SerialiseRequest<GetRequest> for GetRequest {
    fn serialise(&self) -> SerialisedMessage {
        SerialisedMessage::GetRequest(SerialisedRequest::<Self>::new(vec![1, 2, 3]))
    }
}





#[derive(Debug, Clone)]
pub struct GetResponse {
    source: Source,
    destination: Destination,
    serialised_request: SerialisedRequest<GetRequest>,
    error_code: Option<u32>,
    data: Option<String>,
}

impl GetResponse {
    pub fn with_success(get_request: GetRequest,
                        original_request: SerialisedMessage,
                        data: String,
                        close_group_keys: &Vec<PublicKey>) -> GetResponse {
        let serialised_request = match original_request {
            SerialisedMessage::GetRequest(request) => request,
            _ => panic!(),
        };
        GetResponse{ source: get_request.destination().to_group_source(close_group_keys),
                     destination: get_request.source().to_destination(),
                     serialised_request: serialised_request,
                     error_code: None,
                     data: Some(data),
                   }
    }

    pub fn with_failure(get_request: GetRequest,
                        original_request: SerialisedMessage,
                        error_code: u32,
                        close_group_keys: &Vec<PublicKey>) -> GetResponse {
        let serialised_request = match original_request {
            SerialisedMessage::GetRequest(request) => request,
            _ => panic!(),
        };
        GetResponse{ source: get_request.destination().to_group_source(close_group_keys),
                     destination: get_request.source().to_destination(),
                     serialised_request: serialised_request,
                     error_code: Some(error_code),
                     data: None,
                   }
    }

    pub fn data(&self) -> &Option<String> { &self.data }
}

impl Response<GetRequest> for GetResponse {
    fn serialised_request(&self) -> &SerialisedRequest<GetRequest> { &self.serialised_request }
    fn error_code(&self) -> Option<u32> { self.error_code }
}

impl Message for GetResponse {
    fn is_internal() -> bool { false }
    fn source(&self) -> &Source { &self.source }
    fn destination(&self) -> &Destination { &self.destination }
}

impl Accumulate<GetResponse> for GetResponse {
    fn accumulate(_messages: &Vec<Self>) -> Option<Self> { None }
}

impl SerialiseResponse<GetResponse, GetRequest> for GetResponse {
    fn serialise(&self) -> SerialisedMessage {
        SerialisedMessage::GetResponse(SerialisedResponse::<Self, GetRequest>::new(vec![4, 5, 6]))
    }
}




#[derive(Debug, Clone)]
pub enum SerialisedMessage {
    GetRequest(SerialisedRequest<GetRequest>),
    GetResponse(SerialisedResponse<GetResponse, GetRequest>),
}

pub trait SerialiseRequest<T> where T: Request {
    fn serialise(&self) -> SerialisedMessage;
}

pub trait SerialiseResponse<T, U> where T: Response<U>, U: Request {
    fn serialise(&self) -> SerialisedMessage;
}
