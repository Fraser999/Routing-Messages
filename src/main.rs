#![allow(unused)]

#[macro_use]
extern crate log;
#[macro_use]
extern crate maidsafe_utilities;
extern crate rustc_serialize;

mod authority;
mod message_traits;
mod messages;

use authority::*;
use messages::*;
use message_traits::*;


fn parse(serialised_message: SerialisedMessage) {
    match serialised_message {
        SerialisedMessage::GetRequest(request) => println!("Handling Get Request"),
        SerialisedMessage::GetResponse(response) => println!("Handling Get Response"),
    }
}


fn main() {
    let get_request =
        GetRequest::new(Source::Group(GroupAuthority::ClientManager("the source".to_owned()),
                            vec!["src0".to_owned(), "src1".to_owned(), "src2".to_owned()]),
                        Destination::Group(GroupAuthority::NaeManager("the destination".to_owned())),
                        "the data name".to_owned());
    let serialised_get_request = get_request.serialise();
    println!("get_request              : {:?}", get_request);
    println!("get_request.serialise()  : {:?}", serialised_get_request);
    println!("GetRequest::accumulate() : {:?}\n", GetRequest::accumulate(&vec![get_request.clone()]));



    let get_response =
        GetResponse::with_success(get_request, serialised_get_request.clone(), "the data".to_owned(),
                                  &vec!["dst0".to_owned(), "dst1".to_owned(), "dst2".to_owned()]);
    let serialised_get_response = get_response.serialise();

    println!("get_response             : {:?}", get_response);
    println!("get_response.serialise() : {:?}", serialised_get_response);
    println!("GetResponse::accumulate(): {:?}\n", GetResponse::accumulate(&vec![get_response]));


    parse(serialised_get_request);
    parse(serialised_get_response);
}
